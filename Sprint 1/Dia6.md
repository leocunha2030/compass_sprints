-----------------------------------------------------------------------------------
                --> Dia 6 <--
------------------------------------------------------------------------------------

                --> O princípio de Pareto<--
------------------------------------------------------------------------------------

--> O princípio de Pareto, também conhecido como regra 80/20, é uma observação empírica que afirma que aproximadamente 80% dos efeitos são causados por 20% das causas.

--> É amplamente aplicado em diversos campos, como economia, administração, produtividade, qualidade e até mesmo em questões sociais.

--> Ao compreender o princípio de Pareto, as organizações podem direcionar seus recursos de forma mais eficiente, focando nas áreas que têm o maior impacto.

--> Utilizando o princípio de Pareto na testagem de qualidade, a equipe pode otimizar seus esforços, focando nas áreas que são mais relevantes para garantir a satisfação do usuário e a eficiência no processo de desenvolvimento.

<h1>![pareto](https://github.com/leocunha2030/Projeto-Git/assets/134326959/c4d3c89e-1dd1-43d9-bdf4-363febf47c13)</h1>

--> Exemplo de uso do princípio de Pareto na pratica:
    Suponha que uma equipe de desenvolvimento esteja realizando testes em um software e descubra que 20% dos defeitos encontrados são responsáveis por 80% dos problemas relatados pelos usuários. Nesse contexto, a equipe pode aplicar o princípio de Pareto para direcionar seus esforços de testagem de forma mais eficiente. Eles podem priorizar a identificação e correção dos 20% dos defeitos mais críticos, que têm o potencial de resolver a maioria dos problemas relatados pelos usuários.
    Dessa forma, a equipe concentra seus recursos e tempo na resolução dos defeitos que têm o maior impacto, permitindo que entreguem uma versão mais estável e de melhor qualidade do software aos usuários.

----------------------------------------------------------------------------------------
                --> A regra de Myers:<--
----------------------------------------------------------------------------------------

--> A regra 10 de Myers apresenta que o custo da correção de um defeito tende a aumentar quanto mais tarde ele for encontrado. Os defeitos encontrados nas fases iniciais do projeto de desenvolvimento do software são mais baratos do que aqueles encontrados na produção. 

->Exemplo: Se o defeito for encontrado na primeira fase, ele gera 1x de gasto em relação à correção. se for encontrado na próxima fase, já fica 10x, e na outra fase, 100x e assim sucessivamente.

<h1>![myers](https://github.com/leocunha2030/Projeto-Git/assets/134326959/23e69ed6-67a8-4ff3-874f-fe28a518875e) </h1>